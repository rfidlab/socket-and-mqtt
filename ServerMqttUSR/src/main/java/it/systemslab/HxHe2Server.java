package it.systemslab;

import it.systemslab.hxhe2.HxHe2ClientSocket;
import it.systemslab.hxhe2.HxHe2Mqtt;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static it.systemslab.hxhe2.HxHe2Const.*;

public class HxHe2Server {

    private final HxHe2Mqtt helper = new HxHe2Mqtt();

    public static HxHe2Server getInstance() {
        return new HxHe2Server();
    }

    private static void warn(String message) {
        System.out.println(message);
    }

    private static boolean isMultiThread(String[] args) {
        for(String a : args) {
            warn("isMultiThread: " + a);
            if("-mt".equals(a))
                return true;
        }

        return false;
    }

    public static void main(String[] args) {

        HxHe2Server hxHe2Server = getInstance();
        boolean b = false;
        while (!b) {
            if (IS_PROD) b = hxHe2Server.helper.createMqttClient(MQTT_MODE_PROD, MQTT_HOST_PROD, MQTT_PORT_PROD);
            else b = hxHe2Server.helper.createMqttClient(MQTT_MODE_DEV, MQTT_HOST_DEV, MQTT_PORT_DEV);

            if (b)
                warn("Mqtt client created");
            else
                warn("Mqtt client not created");

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        ServerSocket serverSocket =  null;
        try{
            serverSocket = new ServerSocket(8085, MAX_BACKLOG);
            if(isMultiThread(args) ) {
                hxHe2Server.multiThreadConnection(hxHe2Server, serverSocket);
            } else {
                while (true) {
                    warn("waiting for connection...");
                    Socket clientSocket = serverSocket.accept(); //establishes connection
                    warn("connection accepted...");
                    HxHe2ClientSocket hxHe2ClientSocket = new HxHe2ClientSocket(0, clientSocket, hxHe2Server.helper);
                    hxHe2ClientSocket.setSingleThread();
                    hxHe2ClientSocket.start();
                    while (hxHe2ClientSocket.isAlive()) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {

                        }
                    }
                }
            }
        } catch(Exception e){
            warn("while: Unexpected error: " + e);
            e.printStackTrace();
        }

        if(serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                warn("close: Unexpected error: " + e);
                e.printStackTrace();
            }
        }

        hxHe2Server.helper.closeMqttClient();
    }

    ArrayList<HxHe2ClientSocket> sockets = new ArrayList<>(0);

    private void multiThreadConnection(final HxHe2Server hxHe2Server, ServerSocket serverSocket) throws Exception{
        if(serverSocket == null || serverSocket.isClosed())
            throw new IllegalArgumentException("No socket found");

        while (true) {
            warn("waiting for connection...");
            final Socket clientSocket = serverSocket.accept(); //establishes connection
            warn("connection accepted...");
            HxHe2ClientSocket hxHe2ClientSocket = new HxHe2ClientSocket(hxHe2Server.sockets.size(), clientSocket, hxHe2Server.helper);
            hxHe2ClientSocket.setMultithread();
            hxHe2ClientSocket.start();
            hxHe2Server.sockets.add(hxHe2ClientSocket);

            try {
                Thread.sleep(100);
            } catch (Exception e) {

            }

            warn("removing closed connections...");
            hxHe2Server.sockets.removeIf(s -> !s.isAlive());
        }
    }
}
