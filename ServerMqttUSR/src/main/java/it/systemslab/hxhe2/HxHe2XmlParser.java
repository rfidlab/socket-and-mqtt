package it.systemslab.hxhe2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class HxHe2XmlParser {

    private final byte[] payload;

    public HxHe2XmlParser(byte[] payload) {
        this.payload = payload;
    }

    public static HxHe2XmlParser getInstance(byte[] payload) {
        return new HxHe2XmlParser(payload);
    }

    private Element getRootElement() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ByteArrayInputStream input = new ByteArrayInputStream(this.payload);
        Document document = builder.parse(input);

        return document.getDocumentElement();
    }

    private NodeList validateAndGetChildren(Element root, String toCheck) {
        String tagName = root.getTagName();

        if(!toCheck.equals(tagName))
            throw new IllegalArgumentException("Wrong root element: " + tagName);

        NodeList childNodes = root.getChildNodes();
        // System.out.println("tagName: " + tagName + " w/" + childNodes.getLength() + " child");
        return childNodes;
    }

    public String parseTimeSyncMessage() throws ParserConfigurationException, IOException, SAXException {
        Element root = getRootElement();
        NodeList childNodes = validateAndGetChildren(root, "TIME_SYSNC_REQ");
        int childLength = childNodes.getLength();
        for(int i = 0; i <childLength; i++){
            Node item = childNodes.item(i);
            String name = item.getNodeName();

            if("uuid".equals(name))
                return item.getTextContent();
        }

        return null;
    }

    public HxHe2Data parseDataReportMessage() throws ParserConfigurationException, IOException, SAXException {
        HxHe2Data data = new HxHe2Data();

        Element root = getRootElement();
        NodeList childNodes = validateAndGetChildren(root, "UP_SENSOR_DATA_REQ");
        int childLength = childNodes.getLength();

        for(int i = 0; i <childLength; i++){
            Node item = childNodes.item(i);
            String name = item.getNodeName();
            if("uuid".equals(name)) {
                data.setUuid(item.getTextContent());
            } else if("rec_type".equals(name)) {
                data.setRec_type(Integer.parseInt(item.getTextContent()));
            } else if("in".equals(name)) {
                data.setIn(Integer.parseInt(item.getTextContent()));
            } else if("out".equals(name)) {
                data.setOut(Integer.parseInt(item.getTextContent()));
            } else if("time".equals(name)) {
                data.setTime(item.getTextContent());
            } else if("battery_level".equals(name)) {
                data.setBattery_level(Integer.parseInt(item.getTextContent()));
            } else if("warn_status".equals(name)) {
                data.setWarn_status(Integer.parseInt(item.getTextContent()));
            } else if("batterytx_level".equals(name)) {
                data.setBatterytx_level(Integer.parseInt(item.getTextContent()));
            } else if("signal_status".equals(name)) {
                data.setSignal_status(Integer.parseInt(item.getTextContent()));
            } else {
                throw new IllegalArgumentException("Unexpected field: " + name);
            }
        }

        return data;
    }
}
