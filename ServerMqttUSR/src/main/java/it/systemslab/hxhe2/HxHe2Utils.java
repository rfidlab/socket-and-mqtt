package it.systemslab.hxhe2;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class HxHe2Utils {

    private final static byte[] EXPECTED_HEAD = new byte[] {(byte)0xFA, (byte)0xF5, (byte)0xF6};
    private final static byte[] EXPECTED_TAIL = new byte[] {(byte)0xFA, (byte)0xF6, (byte)0xF5};
    public final static byte DATA_REPORT_COMMAND = 0x21;
    public final static byte TIME_SYNC_COMMAND = 0x22;

    private static void warn(String message) {
        System.out.println(message);
    }

    public static boolean checkHead(byte[] head) {
        if(head == null || head.length != 3)
            return false;

        return head[0] == EXPECTED_HEAD[0] && head[1] == EXPECTED_HEAD[1] && head[2] == EXPECTED_HEAD[2];
    }

    public static boolean checkTail(byte[] tail) {
        if(tail == null || tail.length != 3)
            return false;

        return tail[0] == EXPECTED_TAIL[0] && tail[1] == EXPECTED_TAIL[1] && tail[2] == EXPECTED_TAIL[2];
    }

    public static boolean checkTypOfInstruction(byte[] toi) {
        if(toi == null || toi.length != 1)
            return false;

        return (toi[0] == DATA_REPORT_COMMAND) || (toi[0] == TIME_SYNC_COMMAND);
    }

    public static int convertSerialNumber(byte[] sn) {
        if(sn == null || sn.length != 2)
            return -1;

        int result = ((sn[0] << 8) & 0xFF00) + (sn[1] & 0xFF);
        // System.out.println("Serial number: " + result);
        return result;
    }

    public static int convertDataLength(byte[] dl) {
        if(dl == null || dl.length != 2)
            return -1;

        return ((dl[0] << 8) & 0xFF00) + (dl[1] & 0xFF);
    }

    public static String fromBytesToString(byte[] array) {
        if(array == null || array.length == 0)
            return null;

        StringBuilder sb = new StringBuilder();
        for (byte x : array) {
            sb.append(String.format("%02x", x));
        }

        return sb.toString();
    }

    public static byte[] extractData(byte[] source, int length) {
        return Arrays.copyOf(source, length);
    }

    public static byte[] extractHead(byte[] source) {
        /// from is inclusive, to is exclusive
        /// result will be a copy from index 0 to 2
        return Arrays.copyOfRange(source, 0,3);
    }

    public static byte[] extractSerialNumber(byte[] source) {
        return Arrays.copyOfRange(source, 3,5);
    }

    public static byte[] extractTypeOfInstruction(byte[] source) {
        return Arrays.copyOfRange(source, 5,6);
    }

    public static byte[] extractDataLength(byte[] source) {
        return Arrays.copyOfRange(source, 6,8);
    }

    public static byte[] extractPayload(byte[] source) {
        /// check if the length of the result array match the data length
        return Arrays.copyOfRange(source, 8,source.length - 3);
    }

    // faf5f6
    // 0001
    // 21
    // 0029
    // 3c5500

    public static byte[] extractTail(byte[] source) {
        /// from is inclusive, to is exclusive
        /// result will be a copy from index 'length - 3' to 'length - 1'
        return Arrays.copyOfRange(source, source.length - 3, source.length);
    }

    public static byte[] buildDataFullFormatResponse(byte[] sn, String uuid, int ret) {

        byte[] payload = buildDataReportPayload(uuid, ret);
        int len = 3 + 2 + 1 + 2 + payload.length + 3;
        byte[] response = new byte[len];

        // head 3 byte
        response[0] = EXPECTED_HEAD[0];
        response[1] = EXPECTED_HEAD[1];
        response[2] = EXPECTED_HEAD[2];

        // serial number 2 byte
        response[3] = sn[0];
        response[4] = sn[1];

        // toi 1 byte
        response[5] = DATA_REPORT_COMMAND;

        // data length 2 byte
        byte[] dl = convertLength(response.length);
        response[6] = dl[0];
        response[7] = dl[1];

        // payload xmlReponse payload.length byte
        System.arraycopy(payload, 0, response, 8, payload.length);

        // tail 3 byte
        response[len - 3] = EXPECTED_TAIL[0];
        response[len - 2] = EXPECTED_TAIL[1];
        response[len - 1] = EXPECTED_TAIL[2];
        return response;
    }

    public static byte[] buildFullTimeSyncResponse(byte[] sn, byte[] payload) {

        int len = 3 + 2 + 1 + 2 + payload.length + 3;
        byte[] response = new byte[len];

        // head 3 byte
        response[0] = EXPECTED_HEAD[0];
        response[1] = EXPECTED_HEAD[1];
        response[2] = EXPECTED_HEAD[2];

        // serial number 2 byte
        response[3] = sn[0];
        response[4] = sn[1];

        // toi 1 byte
        response[5] = DATA_REPORT_COMMAND;

        // data length 2 byte
        byte[] dl = convertLength(response.length);
        response[6] = dl[0];
        response[7] = dl[1];

        // payload xmlReponse payload.length byte
        System.arraycopy(payload, 0, response, 8, payload.length);

        // tail 3 byte
        response[len - 3] = EXPECTED_TAIL[0];
        response[len - 2] = EXPECTED_TAIL[1];
        response[len - 1] = EXPECTED_TAIL[2];
        return response;
    }

    public static byte[] buildSuccessFullResponse(byte[] sn, String uuid) {
        return buildDataFullFormatResponse(sn, uuid, 0);
    }

    public static byte[] buildFailureFullResponse(byte[] sn, String uuid) {
        return buildDataFullFormatResponse(sn, uuid, 1);
    }

    public static byte[] buildNotRegisteredFullResponse(byte[] sn, String uuid) {
        return buildDataFullFormatResponse(sn, uuid, 2);
    }

    /// 0: success; 1: failure; 2: not registered
    public static byte[] buildDataReportPayload(String uuid, int ret) {
        String response = String.format(
                "<UP_SENSOR_DATA_RES><uuid>%s</uuid><ret>%d</ret></UP_SENSOR_DATA_RES>",
                uuid, ret
        );
        warn("response: " + response);
        return response.getBytes();
    }

    public static byte[] buildTimeSyncPayload(
            String uuid, int ret, String time, String uploadInterval, String dataStartTime, String dataEndTime
    ) {
        String response = String.format(
                "<TIME_SYSNC_RES>" +
                        "<uuid>%s</uuid>" +
                        "<ret>%d</ret>" +
                        "<time>%s</time>" +
                        "<uploadInterval>%s</uploadInterval>" +
                        "<dataStartTime>%s</dataStartTime>" +
                        "<dataEndTime>%s</dataEndTime>" +
                        "</TIME_SYSNC_RES>",
                uuid, ret, time, uploadInterval, dataStartTime, dataEndTime
        );
        warn("response: " + response);
        return response.getBytes();
    }

    private static byte[] convertLength(int length) {
        String hexLength = String.format("%04X", length);
        String firstByte = hexLength.substring(0, 2);
        String secondByte = hexLength.substring(2);
        byte first = (byte) (Short.valueOf(firstByte, 16) & 0xFF);
        byte second = (byte) (Short.valueOf(secondByte, 16) & 0xFF);
        return new byte[]{first, second};
    }

    public static byte[] buildEmptyResponse() {
        return new byte[] {
                EXPECTED_HEAD[0], EXPECTED_HEAD[1], EXPECTED_HEAD[2],
                EXPECTED_TAIL[0], EXPECTED_TAIL[1], EXPECTED_TAIL[2]
        };
    }

    public static byte[] cleanPayload(byte[] payload) {
        String xmlPayload = new String(payload);
        // data polishing
        xmlPayload = xmlPayload
                .replaceAll("\t", "")
                .replaceAll(" ", "")
                .replaceAll("\n", "");
        System.out.println("request: " + xmlPayload);

        return xmlPayload.getBytes();
    }

    public static String buildTime() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return simpleDateFormat.format(now);
    }

    public static boolean checkData(byte[] data) {
        if(data == null || data.length == 0) {
            warn("CheckData: no data");
            return false;
        }

        byte[] head = HxHe2Utils.extractHead(data);
        if(!HxHe2Utils.checkHead(head)){
            warn("Head does not match the format FA F5 F6: " + HxHe2Utils.fromBytesToString(head));
            return false;
        }

        int sn = HxHe2Utils.convertSerialNumber(HxHe2Utils.extractSerialNumber(data));
        if(sn < 0 || sn > 65535) {
            warn("Wrong serial number " + sn);
            return false;
        }

        byte[] toi = HxHe2Utils.extractTypeOfInstruction(data);
        if(!HxHe2Utils.checkTypOfInstruction(toi)) {
            warn("Wrong type of instruction: " + HxHe2Utils.fromBytesToString(toi));
            return false;
        }

        int dataLength = HxHe2Utils.convertDataLength(HxHe2Utils.extractDataLength(data));
        if(dataLength < 0 || dataLength > 65500) {
            warn("Wrong data length: " + dataLength);
            return false;
        }

        byte[] payload = HxHe2Utils.extractPayload(data);
        if(payload.length != dataLength) {
            warn(String.format("Payload length %d and data length fields %d does not match", payload.length, dataLength));
            return false;
        }

        byte[] tail = HxHe2Utils.extractTail(data);
        if(!HxHe2Utils.checkTail(tail)){
            warn("Tail does not match the format FA F6 F5: " + HxHe2Utils.fromBytesToString(tail));
            return false;
        }

        return true;
    }
}
