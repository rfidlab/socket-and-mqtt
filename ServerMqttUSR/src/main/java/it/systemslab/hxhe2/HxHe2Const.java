package it.systemslab.hxhe2;

public class HxHe2Const {

    // with this value we're saying that either the server socket and the mqtt client could handle at most these
    // MAX_BACKLOG connections. Default implementation for server socket is 50.
    // Default implementation for mqtt client is 10.
    public static final int MAX_TRANSMITTER = 30;              // 30
    public static final int MAX_BACKLOG = 4 * MAX_TRANSMITTER; // 120
    public static final int MAX_MQTT = 2 * MAX_BACKLOG;        // 240
    public static final boolean IS_PROD = true;

    public static final String MQTT_MODE_PROD = "tcp";
    public static final String MQTT_HOST_PROD = "stream.lifesensor.cloud";
    public static final String MQTT_PORT_PROD = "8889";

    public static final String MQTT_MODE_DEV = "tcp";
    public static final String MQTT_HOST_DEV = "broker.hivemq.com";
    public static final String MQTT_PORT_DEV = "1883";
}
