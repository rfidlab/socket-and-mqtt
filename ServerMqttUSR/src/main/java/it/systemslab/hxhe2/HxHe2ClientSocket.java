package it.systemslab.hxhe2;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import static it.systemslab.hxhe2.HxHe2Utils.*;
import static it.systemslab.hxhe2.HxHe2Utils.TIME_SYNC_COMMAND;

public class HxHe2ClientSocket {

    private static final int MAX_MESSAGE_SIZE = 65546;
    private final HxHe2Mqtt helper;
    private final int id;
    private final Socket clientSocket;
    private Thread t;
    private boolean stop = false;
    private boolean isSingleThread = true;

    public HxHe2ClientSocket(int id, Socket clientSocket, HxHe2Mqtt helper) {
        this.id = id;
        this.clientSocket = clientSocket;
        this.helper = helper;
    }

    public void setMultithread() {
        this.isSingleThread = false;
    }

    public void setSingleThread() {
        this.isSingleThread = true;
    }

    public boolean isAlive() {
        if(t == null)
            return false;
        return t.isAlive();
    }

    private static void warn(String message) {
        System.out.println(message);
    }

    public void start() {
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!stop) {
                    try {
                        DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
                        warn(id + ": handleSocketBlocking: waiting for data..." + clientSocket);
                        /// this is the blocking version. But, if we add a boolean and a timeout we can convert it in
                        /// the 'non-blocking' version.
                        long now = Calendar.getInstance().getTimeInMillis();
                        while (dis.available() == 0) {
                            if (Calendar.getInstance().getTimeInMillis() - now >= 5000) {
                                if(isSingleThread) {
                                    throw new TimeoutException(id + ": Brute forcing killing the socket" + clientSocket);
                                } else {
                                    break;
                                }
                            } else {
                                Thread.sleep(50);
                            }
                        }
                        handleRequest(dis);

                        // we force the socket closure after the the completion of the request
                        stop = true;
                        Thread.sleep(10+ Math.abs(new Random().nextLong() % 50));
                    } catch (Exception e) {
                        warn("HxHe2ClientSocket error: " + e);
                        e.printStackTrace();
                        break;
                    }
                }

                try {
                    System.out.println("Closing socket " + id + "> " + clientSocket);
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    private void handleRequest(DataInputStream dis) throws Exception {

        byte[] readBytes = new byte[MAX_MESSAGE_SIZE];
        int read = dis.read(readBytes);
        String message = "nothing to report";

        if (read > 1) {
            /// data goes from read data[0] to data[read - 1]
            byte[] data = HxHe2Utils.extractData(readBytes, read);
            if (data.length == read) {
                if (checkData(data)) {
                    handleData(clientSocket, data);
                    return;
                } else {
                    message = "error in checking the data";
                }
            } else {
                message = "error while read data";
            }
        } else {
            throw new Exception("Cannot read the socket. Forcing closure");
        }

        // if you're here then it's an error situation
        warn(message);
        sendEmptyResponse();
    }

    private byte[] handleDataReport(byte[] data) {
        byte[] sn = HxHe2Utils.extractSerialNumber(data);
        HxHe2Data parsedData = null;
        String uuid = null;

        // creating the parser object
        HxHe2XmlParser instance = HxHe2XmlParser.getInstance(
                HxHe2Utils.cleanPayload(HxHe2Utils.extractPayload(data))
        );

        try {
            // parsing data report message
            parsedData = instance.parseDataReportMessage();
            uuid = parsedData.getUuid();
            // no error on parsing data report message
            // validating data report message
            if (parsedData.validate()) {
                // validated data report message
                // sending data to broker
                helper.publish(parsedData.toString());
                // sending response to FC
                return HxHe2Utils.buildSuccessFullResponse(sn, uuid);
            }

            warn("parsed xml data is not valid");
        } catch (Exception e) {
            warn("Error while parsing xml: " + e);
        }

        if (uuid == null) {
            // send empty response
            warn("no uuid. Sending empty response");
            return HxHe2Utils.buildEmptyResponse();
        } else {
            // send failure response
            return HxHe2Utils.buildFailureFullResponse(sn, parsedData.getUuid());
        }
    }

    private byte[] handleTimeSync(byte[] data) {
        HxHe2XmlParser instance = HxHe2XmlParser.getInstance(
                HxHe2Utils.cleanPayload(HxHe2Utils.extractPayload(data))
        );

        byte[] baseResponse = buildEmptyResponse();
        String uuid = null;
        try {
            uuid = instance.parseTimeSyncMessage();
        } catch (Exception e) {
            warn("Error in parsing the message");
        }

        if (uuid != null) {
            return buildFullTimeSyncResponse(
                    extractSerialNumber(data),
                    buildTimeSyncPayload(
                            uuid,
                            0,
                            buildTime(),
                            "0005", // one minute interval
                            "0000", // start monitoring time: midnight
                            "2359"   // end monitoring time: midnight - 1
                    ));
            // hopefully with these stats monitoring should be all day long
        }

        return baseResponse;
    }

    private void handleData(Socket socket, byte[] data) {
        byte[] toi = HxHe2Utils.extractTypeOfInstruction(data);
        byte[] basicResponse = buildEmptyResponse();

        if (toi[0] == DATA_REPORT_COMMAND) {
            basicResponse = handleDataReport(data);
        } else if (toi[0] == TIME_SYNC_COMMAND) {
            basicResponse = handleTimeSync(data);
        } else {
            warn("Unknown command");
        }

        sendResponse(basicResponse);
    }

    public void stop() {
        if(t != null) {
            if (t.isAlive()) {
                this.stop = true;
                while (t.isAlive()) ;
            }
        }

        t = null;
    }

    private void sendResponse(byte[] response) {
        try {
            // warn("sending response: " + response.length + " bytes");
            if (!clientSocket.isClosed())
                clientSocket.getOutputStream().write(response);
            warn("ok");
        } catch (Exception e) {
            warn("Error in sending message: " + e);
        }
    }

    private void sendEmptyResponse() {
        sendResponse(HxHe2Utils.buildEmptyResponse());
    }
}
