package it.systemslab.hxhe2;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Random;

import static it.systemslab.hxhe2.HxHe2Const.MAX_BACKLOG;
import static it.systemslab.hxhe2.HxHe2Const.MAX_MQTT;

public class HxHe2Mqtt implements MqttCallback {

    private MqttClient client;

    public HxHe2Mqtt() {
        
    }

    public boolean createMqttClient(String mode, String host, String port) {
        try {
            String connString = String.format("%s://%s:%s", mode, host, port);
            String clientId = "nuvola_" + new Random().nextInt(1024);
            System.out.printf("connString: %s; clientId: %s%n", connString, clientId);
            MemoryPersistence persistence = new MemoryPersistence();
            client = new MqttClient(connString, clientId, persistence);
            client.setCallback(this);
            client.connect(buildMqttConnectOptions());
            client.subscribe("controllo-accessi", 1);
            return true;
        } catch (MqttException e) {
            client = null;
            System.out.println("Error while creating the mqtt client: " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    public void closeMqttClient() {
        if(client != null) {
            try {
                client.close();
                System.out.println("Client closed");
            } catch (Exception e) {
                System.out.println("Error while closing the mqtt client: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private MqttConnectOptions buildMqttConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        // messaggi che vengono considerati come da inviare e quindi memorizzati sul file finché non arriva una conferma
        // default impostato a 10, può crescere fino a 65535. Idealmente non dovrebbe superare il numero di connessioni
        // gestite col backlog della socket
        mqttConnectOptions.setMaxInflight(MAX_MQTT);
        // https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#setConnectionTimeout-int-
        /*
         * Sets the connection timeout value.
         * This value, measured in seconds, defines the maximum time interval the client will wait for the network connection to the MQTT server to be established.
         * The default timeout is 30 seconds.
         * A value of 0 disables timeout processing meaning the client will wait until the network connection is made successfully or fails.
         * */
        mqttConnectOptions.setConnectionTimeout((int) (2.5 * 60));
        /// https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#setKeepAliveInterval-int-
        /*
         * Sets the "keep alive" interval.
         * This value, measured in seconds, defines the maximum time interval between messages sent or received.
         * It enables the client to detect if the server is no longer available, without having to wait for the TCP/IP timeout.
         * The client will ensure that at least one message travels across the network within each keep alive period.
         * In the absence of a data-related message during the time period, the client sends a very small "ping" message, which the server will acknowledge.
         * A value of 0 disables keepalive processing in the client.
         * */
        mqttConnectOptions.setKeepAliveInterval(5 * 60);
        return mqttConnectOptions;
    }


    synchronized public void publish(String message) {
        MqttMessage mqttMessage = new MqttMessage(message.getBytes());
        mqttMessage.setQos(1);
        mqttMessage.setRetained(true);
        try {
            client.publish("controllo-accessi", mqttMessage);
        } catch (Exception e) {
            System.out.println("Error while publishing message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.out.println("Connection lost: " + throwable);
    }

    private HashSet<String> ids = new HashSet<>();
    private long timestamp = 0;
    private long time = 60000;

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println("messageArrived: topic: " + s);
        System.out.println("messageArrived: mqttMessage: " + new String(mqttMessage.getPayload()));
//        try {
//            JSONObject object = new JSONObject(new String(mqttMessage.getPayload()));
//            String uuid = object.getString("uuid");
//            if (ids.add(uuid)) {
//                // TODO: aggiungere print dei dati
//                ids.forEach(System.out::println);
//            }
//        } catch (Exception e) {
//
//        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        System.out.println("deliveryComplete: " + iMqttDeliveryToken.getMessageId());
    }
}
