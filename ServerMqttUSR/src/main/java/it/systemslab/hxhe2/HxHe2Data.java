package it.systemslab.hxhe2;

public class HxHe2Data {

    private String uuid = null;
    private int rec_type = -1;
    private int in = -1;
    private int out = -1;
    private String time = null;
    private int battery_level = -1;
    private int warn_status = -1;
    private int batterytx_level = -1;
    private int signal_status = -1;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getRec_type() {
        return rec_type;
    }

    public void setRec_type(int rec_type) {
        this.rec_type = rec_type;
    }

    public int getIn() {
        return in;
    }

    public void setIn(int in) {
        this.in = in;
    }

    public int getOut() {
        return out;
    }

    public void setOut(int out) {
        this.out = out;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getBattery_level() {
        return battery_level;
    }

    public void setBattery_level(int battery_level) {
        this.battery_level = battery_level;
    }

    public int getWarn_status() {
        return warn_status;
    }

    public void setWarn_status(int warn_status) {
        this.warn_status = warn_status;
    }

    public int getBatterytx_level() {
        return batterytx_level;
    }

    public void setBatterytx_level(int batterytx_level) {
        this.batterytx_level = batterytx_level;
    }

    public int getSignal_status() {
        return signal_status;
    }

    public void setSignal_status(int signal_status) {
        this.signal_status = signal_status;
    }

    @Override
    public String toString() {
        // json format checker: https://jsonformatter.curiousconcept.com/#
        return "{" +
                "\"uuid\":\"" + uuid + "\"" +
                ",\"rec_type\":" + rec_type +
                ",\"in\":" + in +
                ",\"out\":" + out +
                ",\"time\":\"" + time + "\"" +
                ",\"battery_level\":" + battery_level +
                ",\"warn_status\":" + warn_status +
                ",\"batterytx_level\":" + batterytx_level +
                ",\"signal_status\":" + signal_status +
                '}';
    }

    public boolean validate() {
        if(uuid == null) return false;
        if(time == null) return false;
        if(rec_type == -1 || !(rec_type == 1 || rec_type == 2)) return false;
        if(in < 0) return false;
        if(out < 0) return false;
        if(battery_level < 0 || battery_level > 100) return false;
        if(!(warn_status == 1 || warn_status == 0)) return false;
        if(batterytx_level < 0 || batterytx_level > 100) return false;
        if(!(signal_status == 0 || signal_status == 1)) return false;

        return true;
    }
}
