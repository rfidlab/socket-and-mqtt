//package it.systemslab;
//
//import org.eclipse.paho.client.mqttv3.MqttClient;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//
//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//import java.util.Arrays;
//import java.util.Calendar;
//
//public class USRServer {
//    public static void main(String[] args) throws MqttException {
//        MqttClient client = new MqttClient("tcp://192.168.2.1:1885", "client_3");
//        client.connect();
//
//        try{
//            ServerSocket ss=new ServerSocket(8234);
//            System.out.println("waiting for connection...");
//            Socket s=ss.accept();//establishes connection
//            System.out.println("connection accepted...");
//            DataInputStream dis=new DataInputStream(s.getInputStream());
//            while (true) {
//                byte[] b = new byte[9];
//                System.out.println("waiting for data...");
//                dis.read(b);
//                System.out.print(Calendar.getInstance().getTime().toString() + "> ");
//                for (byte x : b) {
//                    System.out.print(String.format("%02x ", x));
//                }
//
//                if(b[0] == 0x00 && b[1] == 0x01 && b[2] == 0xff){
//                    System.out.print("> registration packet");
//                } else {
//                    try {
//                        byte[] bytes = Arrays.copyOfRange(b, 3, 7);
//                        float f = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getFloat();
//                        System.out.print("> " + f);
//                        String strMessage = String.format(
//                                "{\"alias\":\"SENSOR2\", \"typology\":\"LevelSensor\",\"zone\":1,\"sector\":3,\"value\":%.2f}",
//                                f
//
//                        );
//                        MqttMessage message = new MqttMessage(strMessage.getBytes());
//                        message.setQos(1);
//                        message.setRetained(false);
//                        client.publish(
//                                "sensor",
//                                message
//                        );
//                    } catch (Exception e) {
//                        System.out.print("> " + e.toString());
//                    }
//                }
////                byte[] x = new byte[] { 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, (byte) 0xD5, (byte) 0xCA};
//                byte[] x = new byte[] { 0x01, 0x02, 0x03};
//                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
//                dos.write(x);
//                System.out.println();
//            }
////            ss.close();
//
//        }catch(Exception e){
//            System.out.println(e);
//            e.printStackTrace();
//        }
//
//    }
//}
