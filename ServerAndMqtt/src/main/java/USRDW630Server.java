import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Calendar;

public class USRDW630Server {
    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(8234);
            System.out.println("waiting for connection...");
            Socket s = ss.accept();//establishes connection
            System.out.println("connection accepted...");
            DataInputStream dis = new DataInputStream(s.getInputStream());

            while (true) {
                byte[] b = new byte[9];
                System.out.println("waiting for data...");
                dis.read(b);
                System.out.print(Calendar.getInstance().getTime().toString() + "> ");
                for (byte x : b) {
                    System.out.print(String.format("%02x ", x));
                }

                if (b[0] == 0x00 && b[1] == 0x01 && b[2] == 0xff) {
                    System.out.print("> registration packet");
                } else {
                    try {
                        byte[] bytes = Arrays.copyOfRange(b, 3, 7);
                        float f = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getFloat();
                        System.out.print("> " + f);
                    } catch (Exception e) {
                        System.out.print("> " + e.toString());
                    }
                }
//                byte[] x = new byte[] { 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, (byte) 0xD5, (byte) 0xCA};
                byte[] x = new byte[]{0x01, 0x02, 0x03};
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                dos.write(x);
                System.out.println();
            }
//            ss.close();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
}
