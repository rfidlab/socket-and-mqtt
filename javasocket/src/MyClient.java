import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MyClient {

    private Socket client;
    private PrintWriter out;
    private BufferedReader in;
    private final static byte[] HEAD = new byte[] {(byte)0xFA, (byte)0xF5, (byte)0xF6};
    private final static byte[] TAIL = new byte[] {(byte)0xFA, (byte)0xF6, (byte)0xF5};
    private final static byte DATA_REPORT_COMMAND = 0x21;
    private final static byte TIME_SYNC_COMMAND = 0x22;
    private final static String EXAMPLE_UUID = "1902109290000";
    private final static int EXAMPLE_IN = 2;
    private final static int EXAMPLE_OUT = 3;
    private final static int MAX = 1;
    public static final int MAX_CONNECTIONS = 4; // > 24 * 60 * 15 * 1,04 * 1,22

    public static MyClient getInstance() {
        return new MyClient();
    }

    private int seqNumber = 0;

    private String buildTime() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return String.format("\n\t<time>%s</time>\n",simpleDateFormat.format(now));
    }

    private String buildInOut(int in, int out) {
        return String.format("<in>%d</in><out>%d</out>\n\n", in, out);
    }

    private String buildRecTypeOrSignalStatus(boolean rectype) {
        if(rectype)
            return String.format("\t<rec_type>%d</rec_type>\n", 1 + (new Random().nextInt(1024)) % 2);
        else
            return String.format("\t<signal_status>%d</signal_status>\n", (new Random().nextInt(1024)) % 2);
    }

    private String buildBattery(boolean isTx) {
        if(isTx)
            return String.format("\t<batterytx_level>%d</batterytx_level>\n", (new Random().nextInt(1024)) % 101);
        else {
            int battery = (new Random().nextInt(1024)) % 101;
            int warnStatus = battery <= 25 ? 1 : 0;
            return String.format("\t<battery_level>%d</battery_level>\n\t<warn_status>%d</warn_status>\n", battery, warnStatus);
        }
    }

    private String buildUuid(String uuid) {
        return String.format("\n\t<uuid>%s</uuid>\n", uuid);
    }

    private String buildDataReportPayload(String uuid, int in, int out) {
        return String.format(
                "<UP_SENSOR_DATA_REQ>%s%s%s%s%s%s%s</UP_SENSOR_DATA_REQ>",
                buildUuid(uuid),
                buildRecTypeOrSignalStatus(true),
                buildInOut(in, out),
                buildTime(),
                buildBattery(false),
                buildBattery(true),
                buildRecTypeOrSignalStatus(false)
        );
    }

    private String buildTimeSyncPayload() {
        return String.format(
                "<TIME_SYSNC_REQ>%s</TIME_SYSNC_REQ>",
                buildUuid(EXAMPLE_UUID)
        );
    }

    private byte[] buildMessage() {
        byte[] data = new byte[65600];

        // head
        data[0] = HEAD[0];
        data[1] = HEAD[1];
        data[2] = HEAD[2];

        // serial number
        byte[] sn = convertLength(seqNumber);
        seqNumber += 1;
        data[3] = sn[0];
        data[4] = sn[1];

        // type of instruction
        data[5] = TIME_SYNC_COMMAND;
        String payload = buildDataReportPayload(UUID.randomUUID().toString(), EXAMPLE_IN, EXAMPLE_OUT);
        if(data[5] == TIME_SYNC_COMMAND)
            payload = buildTimeSyncPayload();

        // data length
        byte[] dl = convertLength(payload.length());
        data[6] = dl[0];
        data[7] = dl[1];

        int len = 3 + 2 + 1 + 2 + payload.length() + 3;

        // payload
        byte[] result = payload.getBytes(StandardCharsets.UTF_8);
        System.arraycopy(result, 0, data, 8, result.length);

        data[len - 3] = TAIL[0];
        data[len - 2] = TAIL[1];
        data[len - 1] = TAIL[2];

        return Arrays.copyOfRange(data, 0, len);
    }

    private byte[] convertLength(int length) {
        String hexLength = String.format("%04X", length);
        String firstByte = hexLength.substring(0, 2);
        String secondByte = hexLength.substring(2);
        byte first = (byte) (Short.valueOf(firstByte, 16) & 0xFF);
        byte second = (byte) (Short.valueOf(secondByte, 16) & 0xFF);
//        System.out.printf("%d > %s > 0x%s 0x%s > %d %d %n", length, hexLength, firstByte, secondByte, first, second);
        return new byte[]{first, second};
    }

    private void testLengths() {
        for(int i = 255; i < 1024; i++) {
            convertLength(i);
        }
    }

    public boolean startConnection(String ip, int port) {
        try {
//            System.out.println("starting connection");
            client = new Socket(ip, port);
//            System.out.println("Socket");
            out = new PrintWriter(client.getOutputStream(), true);
//            System.out.println("PrintWriter");
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
//            System.out.println("BufferedReader");
        } catch (Exception e) {
            System.out.println("startConnection: Error " + e.getMessage());
            return false;
        }

        return true;
    }

    public String sendMessage() {
        try {
            byte[] payload = buildMessage();
//            System.out.printf("sending _%s_%n", fromBytesToString(payload));
            client.getOutputStream().write(payload);
            char[] buff = new char[65546];
            int read = in.read(buff);
            int offset = 3 + 2 + 1 + 2 + 3; // head, sn, toi, dl, tail
            char[] response = new char[read-offset];
            System.arraycopy(buff, 8, response, 0, read - offset);
            return new String(response);
        } catch (Exception e) {
//            System.out.println("Error in sending message");
            return e.toString();
        }
    }

    public void stopConnection() {
        try {
//            System.out.println("Closing connection");
            in.close();
            out.close();
            client.close();
        } catch (Exception e) {
            System.out.println("stopConnection: " + e.toString());
        }
    }

    public static String fromBytesToString(byte[] array) {
        if(array == null || array.length == 0)
            return null;

        StringBuilder sb = new StringBuilder();
        for (byte x : array) {
            sb.append(String.format("%02x", x));
        }

        return sb.toString();
    }

    static final AtomicInteger error = new AtomicInteger();
    static final AtomicInteger success = new AtomicInteger();
    static final AtomicInteger connectionError = new AtomicInteger();

    static byte[] fromStringToByte(String message) {
        for(int i = 0; i < message.length(); i++) {
            String sByte = message.substring(i, i+2);
            System.out.println(sByte);
            i+=1;
        }

        return null;
    }

    public static void main(String[] args) {
//        mc.testLengths();

        System.out.println("ready");
        ArrayList<Thread> threads = new ArrayList<>();
        for(int i = 0; i < MAX_CONNECTIONS; i++) {
            int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10+ Math.abs(new Random().nextLong() % 60));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    MyClient mc = getInstance();
                    if(mc.startConnection("localhost", 8085)) {
                        String r = mc.sendMessage();
                        if (r.contains("<ret>0</ret>")) {
                            success.addAndGet(1);
                        } else {
                            System.out.println(finalI + ": response: " + r);
                            error.addAndGet(1);
                        }

                        mc.stopConnection();
                    } else {
                        connectionError.addAndGet(1);
                    }
                }
            });

            threads.add(t);
        }
        System.out.println("set");
        System.out.println("go");

        try {
            threads.forEach(Thread::start);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        long count = threads.stream().filter(Thread::isAlive).count();
        while(count > 0) {
            count = threads.stream().filter(Thread::isAlive).count();
            System.out.printf(
                "\r%03d/%d threads > success %4d error %4d connectionError %4d",
                count, MAX_CONNECTIONS, success.get(), error.get(), connectionError.get()
            );
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\nsuccess:" + success + "; error:" + error + "; connectionError:" + connectionError);
    }
}