import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class TpdClient {

     private static final String GCP_URL = "34.77.206.194";
     // private static final String LOCAL_URL = "151.100.179.70";
     private static final String LOCAL_URL = "localhost";
     private static final String URL = LOCAL_URL;
    private static final int PORT = 5239;

    private Socket client;
    private PrintWriter out;
    private BufferedReader in;

    // 2022-04-27 16:13:54.666
    // 2022-04-27 16:13:55.057

    public static TpdClient getInstance() {
        return new TpdClient();
    }

    public boolean startConnection(String ip, int port) {
        try {
//            System.out.println("starting connection");
            client = new Socket(ip, port);
//            System.out.println("Socket");
            out = new PrintWriter(client.getOutputStream(), true);
//            System.out.println("PrintWriter");
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
//            System.out.println("BufferedReader");
        } catch (Exception e) {
            System.out.println("startConnection: Error " + e.getMessage());
            return false;
        }

        return true;
    }

    public void stopConnection() {
        try {
//            System.out.println("Closing connection");
            in.close();
            out.close();
            client.close();
        } catch (Exception e) {
            System.out.println("stopConnection: " + e);
        }

    }

    public static String fromBytesToString(byte[] array) {
        if(array == null || array.length == 0)
            return null;

        StringBuilder sb = new StringBuilder();
        for (byte x : array) {
            sb.append(String.format("%02x", x));
        }

        return sb.toString();
    }

    // 41.905417
    // 12.515634°
    // official 354679095321645
    // unofficial 354 679 095 321 666
    // Via augusto murri
    // Ennfu:183114.00,A,4154.45147,N,01230.96185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01
    private final String outside1  = "Ennfu:183114.00,A,4154.86950,N,01231.88189,E,0.539,,270422#354679095321666#24#3.72#37.6#0#V0.01";
    private final String outside11 = "Ennfu:183114.00,A,4154.40147,N,01230.96185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String iss4      = "Ennfu:183114.00,A,4154.22550,N,01231.17500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String iss5      = "Ennfu:183114.00,A,4154.21550,N,01231.19500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String iss6      = "Ennfu:183114.00,A,4154.19550,N,01231.22500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String cimi      = "Ennfu:CIMI46004559551395CGREG:2,1,5000,8569,3#354679095321666#24#4.06#85.5#V0.01";
    private final String cimi0     = "Ennfu:CIMI46004559551395CGREG:2,1,5000,8569,3#354679095321666#24#4.06#85.5#0#V0.01";
    // via alfonso borelli
    // Ennfu:183114.00,A,4154.40147,N,01230.96185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01
    // viale dell'università
    // Ennfu:183114.00,A,4154.35147,N,01230.96185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01
    private final String outside2  = "Ennfu:183114.00,A,4154.35147,N,01230.96185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    // via del castro laurenziano
    // Ennfu:183114.00,A,4154.35147,N,01231.00185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01
    private final String outside22 = "Ennfu:183114.00,A,4154.35147,N,01231.00185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String outside23 = "Ennfu:183114.00,A,4154.35147,N,01231.01185,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    // via del castro laurenziano
    // fine iss
    private final String iss  = "Ennfu:183114.00,A,4154.25350,N,01231.14500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String iss2 = "Ennfu:183114.00,A,4154.24550,N,01231.15500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String iss3 = "Ennfu:183114.00,A,4154.23550,N,01231.16500,E,0.474,,270422#354679095321666#17#3.92#67.0#0#V0.01";
    private final String heartbeat = "HeartBeat1";
    private final String[] tracks = new String[] {
        outside1, // out
        outside11,
        iss4,
        iss5,
        iss6,
        cimi,
        cimi0,
//            outside11, // out
////            heartbeat,
//            outside11, // out
////            heartbeat,
////            outside2, // out
////            heartbeat,
//            outside22, // in
////            heartbeat,
//            outside23, // in
//            heartbeat,
//            iss,  // out
//            heartbeat,
//            iss2,  // out
////            heartbeat,
//            iss3,  // out
////            heartbeat,
//            iss4,  // out
////            heartbeat,
//            iss5,  // out
//            iss6,  // out
    };

    static final AtomicInteger connectionError = new AtomicInteger();

    static byte[] fromStringToByte(String message) {
        byte[] data = new byte[message.length()];
        for(int i = 0; i < message.length(); i++) {
            data[i] = (byte) message.charAt(i);
        }

        return data;
    }

    public static void main(String[] args) throws InterruptedException {
        TpdClient mc = getInstance();

        if(mc.startConnection(URL, PORT)) {
            int i = 0;
            do {
                for(String s : mc.tracks) {
                    String recv = mc.sendMessage(s);
                    System.out.printf("recv: %s\n%n", recv);
                    Thread.sleep(10000);
                }
                i++;
            } while (i < PORT);

            mc.stopConnection();
        } else {
            connectionError.addAndGet(1);
        }
    }

    private String sendMessage(String s) {
        System.out.println("Sending: " + s);
        byte[] data = fromStringToByte(s);
        try {
            System.out.println("bytes: " + fromBytesToString(data));
            client.getOutputStream().write(data);
            // System.out.println("sent");
            int length = client.getInputStream().available();
            while (length <= 0) {
                length = client.getInputStream().available();
            }
            // System.out.printf("expecting %d bytes%n", length);
            byte[] buff = client.getInputStream().readNBytes(length);
            return new String(buff);
        } catch (Exception e) {
//            System.out.println("Error in sending message");
            return e.toString();
        }
    }
}