## Progetti da controllare
 - `ServerMqttUSR` per la parte server che va installata sulla raspberry
 - `javasocket` per la parte che simula le fotocellule


## Generare file .jar
1. Aprire il progetto con IntelliJ
2. Sulla destra della pagina dovrebbe esserci il tab per maven. Eseguire `clean`
 e poi `package`
3. Nella cartalla `<root del progetto>/target` verrò creato un file chiamato
`ServerMqttUSR-1.0-SNAPSHOT.jar`

## Eseguire il jar
Da shell digitare il comando `java -jar ServerMqttUSR-1.0-SNAPSHOT.jar` per la
modalità single thread. Per la modalità multithread usare il comando
`java -jar ServerMqttUSR-1.0-SNAPSHOT.jar -mt`

## TODO
1. Creare il .service e tutto il necessario per eseguire il ServerMqttUSR.jar
all'avvio della raspberry e al recupero in caso di crash
2. Giocare con i metodi di `javasocket` per creare una sorta di percorso fra le
fotocellule
3. Ogni fotocellula dovrà avere la sua socket. Per testare la modalità
multithread, eseguire il jar con il parametro `-mt` e creare **tutte** le socket
prima del loop per l'invio dei messaggi e chiuderle tutte al termine del loop.
Per testare la modalità single thread le socket devono essere create subito
prima dell'invio del messaggio alla raspberry.
